export class TouristPoints {
    constructor() {
        this.newItem = null;
        this.image = null;

        this.selectors();
        this.events();
    }

    selectors() {
        this.form = document.querySelector(".input-form");
        this.imageInput = document.querySelector(".input-image");
        this.imageLabel = document.querySelector(".input-form-image-label");
        this.titleInput = document.querySelector(".input-form-title");
        this.descriptionInput = document.querySelector(".input-form-description");
        this.result = document.querySelector(".card-result");
    }

    events() {
        this.form.addEventListener("submit", this.addItemToList.bind(this));
        this.imageInput.addEventListener("change", this.addToPreview.bind(this));
        this.imageLabel.addEventListener("dragover", this.dragToPreview.bind(this));
        this.imageLabel.addEventListener("drop", this.addToPreview.bind(this));
    }

    dragToPreview(event) {
        event.preventDefault();
    }

    addToPreview(event) {
        event.preventDefault();
        let file =
            typeof event.dataTransfer !== "undefined" && event.dataTransfer.files.length
                ? event.dataTransfer.files[0]
                : this.imageInput.files[0];
        let reader = new FileReader();
        reader.readAsDataURL(file);
        reader.addEventListener("load", () => {
            this.imageLabel.innerHTML = "";
            this.image = reader.result;
            this.imageLabel.style.backgroundImage = `url("${this.image}")`;
        });
    }

    addItemToList(event) {
        event.preventDefault();
        const title = event.target[1].value;
        const description = event.target[2].value;
        const image = this.image;
        if (title !== "") {
            this.newItem = {
                title: title,
                description: description,
                image: image,
            };

            this.renderLilstItems();
            this.resetInputs();
        }
    }

    renderLilstItems() {
        let itemStructure = this.result.innerHTML;

        itemStructure += `
            <div class="card">
            <img class="card-image" src="${this.newItem.image}"></img>
            <div class="card-content">
            <p class="card-title">${this.newItem.title}</p>
            <p class="card-description">${this.newItem.description}</p>
            </div>
            </div>
            `;
        this.result.innerHTML = itemStructure;
        this.newItem = null;
    }

    resetInputs() {
        this.titleInput.value = "";
        this.descriptionInput.value = "";
        this.imageInput.src = "";
        this.imageLabel.innerHTML = "Imagem";
        this.imageLabel.style.backgroundImage = "";
    }
}
