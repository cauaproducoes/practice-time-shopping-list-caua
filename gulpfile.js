const { src, dest, watch } = require("gulp");
const sass = require("gulp-sass")(require("sass"));
const browserify = require("browserify");
const babelify = require("babelify");
const source = require("vinyl-source-stream");
const uglify = require("gulp-uglify");
const buffer = require("vinyl-buffer");
const image = require("gulp-imagemin");

const paths = {
    html: {
        all: "src/templates/**/*.html",
    },

    styles: {
        all: "src/styles/**/*.scss",
        main: "src/styles/main.scss",
    },

    scripts: {
        all: "src/scripts/**/*.js",
        main: "src/scripts/app.js",
    },

    images: {
        all: "src/assets/images/**/*",
        images_output: "dist/images/",
    },

    output: "dist",
};

function styles() {
    return src(paths.styles.main)
        .pipe(sass({ outputStyle: "compressed" }).on("error", sass.logError))
        .pipe(dest(paths.output));
}

function scripts() {
    return (
        browserify(paths.scripts.main)
            .transform(
                babelify.configure({
                    presets: ["@babel/preset-env"],
                })
            )
            .bundle()
            .pipe(source("bundle.js"))
            //.pipe(buffer())
            //.pipe(uglify())
            .pipe(dest(paths.output))
    );
}

function images() {
    return src(paths.images.all).pipe(image()).pipe(dest(paths.images.images_output));
}

function html() {
    return src(paths.html.all).pipe(dest(paths.output));
}

function auto_load() {
    watch(paths.styles.all, { ignoreInitial: false }, styles);
    watch(paths.scripts.all, { ignoreInitial: false }, scripts);
    watch(paths.html.all, { ignoreInitial: false }, html);
    watch(paths.images.all, { ignoreInitial: false }, images);
}

exports.default = auto_load;
